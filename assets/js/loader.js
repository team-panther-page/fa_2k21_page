page_load_percent = 0;

function timeout_trigger() {
    $("#loader_progress").css("width", page_load_percent + "%");
    if (page_load_percent != 100) {
        setTimeout('timeout_trigger()', 50);
        if (page_load_percent > 50) {
            $('#overlay').css('opacity', (100 - page_load_percent) * 0.1);
        }
    } else {
        $('#overlay').remove();
    }
    page_load_percent++;
}

timeout_trigger();

$(document).ready(function () {
    setTimeout(() => {
        page_load_percent = 80;
    }, 500);
});