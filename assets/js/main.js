$header_slide_images = [
    "assets/images/header_bg_1.jpg",
    "assets/images/header_bg_2.jpg",
];

$active_header_slide = 0;

$(document).ready(function () {
    let $length = $header_slide_images.length;
    $('#n_count').html($length < 10 ? "0" + $length : $length);
    changeBackground();

    $('#scroll_down').click(() => {
        $('html, body').animate({
            scrollTop: $(".devNiainaNarindra").offset().top
        }, 2000);
    });

});

function changeBackground() {
    $('#n_active').html($active_header_slide < 9 ? "0" + ($active_header_slide + 1) : ($active_header_slide + 1));

	// $('header.header').css('background-image', "url(" + $header_slide_images[$active_header_slide] + ")");

	$('header.header').css('background-image', "url(" + $header_slide_images[$active_header_slide] + ")");
	$('header').addClass('opacityBackground');

	setTimeout(() => {
		$('header').removeClass('opacityBackground');
	}, 1999);
	
    $percent = (($active_header_slide) * 100) / ($header_slide_images.length - 1);
    $('#progress_percent').css('width', ($percent).toFixed(2) + '%');

    if ($header_slide_images[($active_header_slide + 1)]) {
        $('#progress_percent').css('transition', "0.5s");
    }

    setTimeout(() => {
        if (!$header_slide_images[++$active_header_slide]) {
            $active_header_slide = 0;
            $('#progress_percent').css('transition', '0s');
        }
        changeBackground();
    }, 1000 * 5);
}

( function() {
	var $carousels, $slideScrollers, $thumbnailScrollers, $nextBtns, $prevBtns;
	const activeThumbnail = 'active', scrollingThumbnails = 'scrolling', disabledBtns = 'disabled';

	$( document ).ready( function() {
		$carousels = $( '.carousel' );
		
		$slideScrollers = $carousels.find( '.slide-container' );
		$thumbnailScrollers = $carousels.find( '.thumb-container' );
		
		$nextBtns = $carousels.find( '.next' ).on( 'click', nextSlide );
		$prevBtns = $carousels.find( '.prev' ).on( 'click', prevSlide );
		$slideScrollers.on( 'scroll', scrollHandler );
		
		$carousels.each( function() {
			updateIndicators( $( this ) );
			updateArrows( $( this ) );
		});

		$(".mini-slide").on("click",function(){
            $("#section-mini-slide").fadeOut(500);
            $("#section-lg-slide").fadeIn(500);
        })
        $("#section-lg-slide").hide() 
            
        $("#hide-section-lg-slide").on("click",function(){
            $("#section-lg-slide").fadeOut(500);
            $("#section-mini-slide").fadeIn(500);
        })

	});
	
	function nextSlide( e ) {
		e.preventDefault();
		var $carousel = $( this ).closest( $carousels );
		scrollToSlide( $carousel, $carousel.data( 'currIndex' ) + 1 );		
	}
	
	function prevSlide( e ) {
		e.preventDefault();
		var $carousel = $( this ).closest( $carousels );
		scrollToSlide( $carousel, $carousel.data( 'currIndex' ) - 1 );
	}
	
	function scrollHandler() {
		var $carousel = $( this ).closest( $carousels );
		updateIndicators( $carousel );
		updateArrows( $carousel );
	}
	
	function scrollToSlide( $carousel, index ) {
		var $container = $carousel.find( $slideScrollers );
		var $slides = $container.find( 'li' );
		var goToIndex = Math.max( Math.min( index, $slides.length - 1 ), 0 ); // make sure the index is within the bounds of the slideshow

		if( $carousel.data( 'currIndex' ) == goToIndex ) { return; } // bail - no need to update the DOM
		
		var scrollTo = $slides.eq( goToIndex ).get( 0 ).offsetLeft - $container.get( 0 ).offsetLeft;
		$container.get( 0 ).scrollTo( scrollTo, 0 );
		updateIndicators( $carousel, index );
		updateArrows( $carousel );
	}
	
	function getCurrentSlide( $carousel ) {
		var $container = $carousel.find( $slideScrollers );
		var scrollPos = $container.get( 0 ).scrollLeft;
		var containerOffset = $container.get( 0 ).offsetLeft;
		
		var $slides = $container.find( 'li' );
		var index = 0;
		
		$slides.each( function( i ) {
			var slidePos = this.offsetLeft - containerOffset;
			if( slidePos <= scrollPos ) {
				index = i;
			} else {
				return false; // bail out, we've passed the scroll position
			}
		} );
	
		return index;
	}
	
	function updateIndicators( $carousel, index ) {
		if( typeof index == 'undefined' ) {
			index = getCurrentSlide( $carousel );
		}
		
		if( $carousel.data( 'currIndex' ) == index ) { return; } // bail - no need to update the DOM
		
		var $indicatorScroller = $carousel.find( $thumbnailScrollers );
		var $indicators = $indicatorScroller.find( 'li' );
		var $currIndicator = $indicators.eq( index );

		$indicators.find( 'a' ).removeClass( activeThumbnail );
		$currIndicator.find( 'a' ).addClass( activeThumbnail );

		$carousel.data( 'currIndex', index);
		
		// Scroll the indicators to center the one that is active
		var indicatorPos = $currIndicator.get( 0 ).offsetLeft - $indicatorScroller.get( 0 ).offsetLeft;
		var scrollerWidth = $indicatorScroller.width();
		var scrollTo = -(scrollerWidth / 2) + indicatorPos + ( $currIndicator.width() / 2 );
		$indicatorScroller.addClass( scrollingThumbnails ).animate( {
			scrollLeft: scrollTo
		}, 400, function() {
			$indicatorScroller.removeClass( scrollingThumbnails );
		} );
	}
	
	function updateArrows( $carousel ) {
		var $slides = $carousel.find( $slideScrollers ).find( 'li' );
		var $next = $carousel.find( $nextBtns );
		var $prev = $carousel.find( $prevBtns );
		var currIndex = $carousel.data( 'currIndex' );
		
		if( currIndex >= $slides.length - 1 ) {
			$next.addClass( disabledBtns );
		} else if( $next.hasClass( disabledBtns ) ) {
			$next.removeClass( disabledBtns );
		}
		
		if( currIndex <= 0 ) {
			$prev.addClass( disabledBtns );
		} else if( $prev.hasClass( disabledBtns ) ) {
			$prev.removeClass( disabledBtns );
		}
	}
})();
